import React, { Component } from 'react';
import { Col, Nav, NavItem } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import { Link } from 'react-router-dom'


class Sidebar extends Component {

  render() {
    return (
      <Col md="2 bg-light sidebar admin-nav">
        <Nav vertical pills >
          <NavItem>
              <Link to='/admin' className="nav-link">
                <FontAwesome 
                name="tachometer"
                />
              Dashboard
              </Link>            
          </NavItem>
          <NavItem>
              <Link to='/admin/posts' className="nav-link">
                <FontAwesome 
                name="file-text"
                />
              Posts
              </Link>  
          </NavItem>
          <NavItem>
              <Link to='/admin/users' className="nav-link">
                <FontAwesome 
                name="users"
                />
                Users
              </Link>  
          </NavItem>
          <NavItem>
              <Link to='/admin/settings' className="nav-link">
                <FontAwesome 
                name="cogs"
                />
                Settings
              </Link>            
          </NavItem>
        </Nav>
      </Col>
    )
  }
}

export default Sidebar