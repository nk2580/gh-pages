import React from 'react';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container,
 } from 'reactstrap';
import { 
  animateScroll as scroll,
 } from 'react-scroll'


 /**
  * custom smooth scroll move
  * @param int h 
  */
 const smoothScroll = (h) => {
  let i = h || 0;
  if (i < 200) {
    setTimeout(() => {
      window.scrollTo(0, i);
      smoothScroll(i + 10);
    }, 10);
  }
}

/**
 * Navigation - component for the navigation menu
 */
class AdminNavigation extends React.Component {
  constructor(props) {
    super(props);
    // bind the class methods
    this.toggle = this.toggle.bind(this);
    this.scrollToTop = this.scrollToTop.bind(this);
    // set default state
    this.state = {
      isOpen: false
    };
  }

  /**
   * toggle - toggels the open state of the collapsable menu
   * @returns void
   */
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  /**
   * scrollToTop - debugging
   */
  scrollToTop() {
    scroll.scrollToTop();
  }

  /**
   * handle the Logout Action
   * @param string ref 
   */
  handleLogout() {

  }  

  /**
   * render - renders JSX
   * @returns void
   */
  render() {
    return (
      <Navbar className="bg-dark" dark expand="md" fixed="top">
        <Container fluid>
          <NavbarBrand
           onClick={this.scrollToTop}
           className="text-white"
          >
            Admin Panel - Nik Kyriakidis
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
              { 
                !this.props.isLoggedIn
                  ? null
                  : (
                  <Nav className="ml-auto" navbar>
                    <NavItem>
                      <NavLink>
                        <Link to="/">
                        Exit Admin
                        </Link>
                      </NavLink>
                    </NavItem>
                  </Nav>
                  )
              }
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}

AdminNavigation.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
}

export default AdminNavigation;