import React from 'react';

const AdminContainer = (props) => {
  return (
      <div className="app-container">
        {props.children}
      </div>
    );
}

export default AdminContainer;