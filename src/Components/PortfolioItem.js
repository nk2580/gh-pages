import React from 'react';
import PropTypes from 'prop-types'
import { 
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
 } from 'reactstrap';

const PortfolioItem = (props) => {
  return (
    <div>
      <Card>
        <CardImg top src={props.image} className="img-fluid"  />
        <CardBody>
          <CardTitle>{props.title}</CardTitle>
          <CardSubtitle>{props.subTitle}</CardSubtitle>
          <CardText>{props.description}</CardText>
          <a href={props.link}>
            <Button>View Project</Button>
          </a>
        </CardBody>
      </Card>
    </div>
  );
};

PortfolioItem.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string,
  subTitle: PropTypes.string,
  description: PropTypes.string,
  link: PropTypes.string,
};


export default PortfolioItem