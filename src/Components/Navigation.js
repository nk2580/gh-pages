import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container,
 } from 'reactstrap';
 import { 
  animateScroll as scroll,
 } from 'react-scroll'
 import scrollToElement from 'scroll-to-element';

 /**
  * custom smooth scroll move
  * @param int h 
  */
 const smoothScroll = (h) => {
  let i = h || 0;
  if (i < 200) {
    setTimeout(() => {
      window.scrollTo(0, i);
      smoothScroll(i + 10);
    }, 10);
  }
}

/**
 * Navigation - component for the navigation menu
 */
export default class Navigation extends React.Component {
  constructor(props) {
    super(props);
    // bind the class methods
    this.toggle = this.toggle.bind(this);
    this.scrollToTop = this.scrollToTop.bind(this);
    // set default state
    this.state = {
      isOpen: false
    };
  }

  /**
   * toggle - toggels the open state of the collapsable menu
   * @returns void
   */
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  /**
   * scrollToTop - debugging
   */
  scrollToTop() {
    scroll.scrollToTop();
  }

  /**
   * 
   * @param string ref 
   */
  handleScrollToElement(ref) {
    scrollToElement(`#${ref}`, {
      offset: -50,
  });
  }  

  /**
   * render - renders JSX
   * @returns void
   */
  render() {
    return (
      <Navbar className="bg-dark" dark expand="md" fixed="top">
        <Container>
          <NavbarBrand
           onClick={this.scrollToTop}
           className="text-white"
          >
            Nik Kyriakidis
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink
                  onClick={() => this.handleScrollToElement("About")}
                >
                  About Me
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  onClick={() => this.handleScrollToElement("Projects")}
                >
                  Projects
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}
