import React from 'react';
import PropTypes from 'prop-types'
import { Parallax } from 'react-parallax';

const ParallaxDivider = (props) => {
  return (
    <Parallax bgImage={props.bg} strength={400}>
        {props.children}
    </Parallax>
  );
};

ParallaxDivider.propTypes = {
  bg: PropTypes.string.isRequired,
};

export default ParallaxDivider