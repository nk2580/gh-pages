import React from 'react';
import PropTypes from 'prop-types'
import { Row, Col } from 'reactstrap';
import PortfolioItem from './PortfolioItem';

const Portfolio = (props) => {
  return (
    <Row>
      {props.items.map((data, index) => (
        <Col xs="12" md="6" key={index}>
          <PortfolioItem
            title={data.title}
            image={data.image}
            subTitle={data.subTitle}
            description={data.description}
            link={data.link}
            />
        </Col>
      ))}
    </Row>
  );
};

Portfolio.propTypes = {
  items: PropTypes.array.isRequired,
};

export default Portfolio