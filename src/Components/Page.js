import React from 'react';
import PropTypes from 'prop-types';

const Page = (props) => {
  return (
    <div id={props.id}>
        {props.children}
    </div>
  );
};

Page.propTypes = {
  id: PropTypes.string,
}

export default Page