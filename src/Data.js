import husk from './Assets/img/husk.jpg';
import ink from './Assets/img/ink.jpg';
import wordsmith from './Assets/img/wordsmith.jpg';
import wbskt from './Assets/img/wbskt.jpg';

// Protfolio Data
export const PortfolioItems = [
  {
    title: "Nik Kyriakidis - Web Developer",
    image: wbskt,
    subTitle: "",
    description: "Built with React this portfolio site uses a variety of NPM modules to build its content in CI and publish to GitHub for public viewing.",
    link: "https://gitlab.com/nk2580/gh-pages",
  },
  {
    title: "Husk Distillers",
    image: husk,
    subTitle: "",
    description: "Ink gin is a branded site of husk distillers showcasing their gin brand. Built using the WordSmith Framework, the network of sites run a customised version of WordPress. This includes a modularised page builder and resource system based on AngularJS and Laravel’s Eloquent ORM.",
    link: "https://huskdistillers.com/",
  },
  {
    title: "Ink Gin",
    image: ink,
    subTitle: "",
    description: "Husk Distillers site’s were created with the purpose of showcasing their various products and facilitating online sales. Built using the WordSmith Framework, the network of sites run a customised version of WordPress. This includes a modularised page builder and resource system based on AngularJS and Laravel’s Eloquent ORM.",
    link: "https://inkgin.com/",
  },
  {
    title: "WordSmith",
    image: wordsmith,
    subTitle: "Modern Wordpress Framework",
    description: "WordSmith is a Modern PHP Development Framework for WordPress Developers. By implementing an OOP interface for developing WordPress Themes and Plugins it provides an environment for developers to seamlessly integrate modern techniques into a typical WordPress install.",
    link: "http://nk2580.github.io/WordSmith/",
  },
];

// Linkedin Profile URI
export const links = [
  {
    icon: "github-square",
    link: 'https://github.com/nk2580'
  },
  {
    icon: "linkedin-square",
    link: 'https://www.linkedin.com/in/nikkyriakidis/'
  },
  {
    icon: "gitlab",
    link: 'https://gitlab.com/users/nk2580'
  },
];

export const aboutMe = {
  desciption: "Proven people manager with over 5 years professional experience. obsessed with delivery, ready to hit the ground running and expand on current skills. With a fierce interest in coding, keen to explore creative solutions to business problems. With a genuine passion to make a difference in the world through the magic of code, and constantly striving to revise practices to ensure the latest standards and frameworks to are utilised to achieve this goal. With an ability to excel in both individual and collaborative work I feel confident I will work well in any team or project to acheive the ultimate goal of delivery.",
  languages: [
    "PHP",
    "Java",
    "Go",
    "C#",
    "Javascript",
    "HTML",
    "CSS",
    "SASS",
    "SQL",
  ],
  frameworks: [
    "Laravel",
    "Composer",
    "Git",
    "Android",
    "NodeJS",
    "Docker",
    "Angular",
    "React",
    "Redux",
    "WebPack",
    "Gulp",
    "Jest",
    "Selenium",
    "EC2",
    "Kubernetes",
    "Lambda",
  ],
  recommendations: [
    {
      text: "\"Nik is one of the most hard working people I've had the pleasure of working with. He is extremely communicative and always available when we need him, even outside office hours. Nik has constantly gone above and beyond, working diligently to ensure tight deadlines are met. His passion for web developing is evident to anyone who works with him.\"",
      author: "Harriet Messenger - Sales & Marketing Manager @ Husk Distillers",
    },
  ],
}
