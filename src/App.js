import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import AppContainer from './Components/AppContainer';
import Navigation from './Components/Navigation';
import Home from './Pages/Home';
import About from './Pages/About';
import Projects from './Pages/Projects';
import Contact from './Pages/Contact';
import Admin from './Admin';

const AdminContent = (props) => (
  <Admin {...props} />
);

const PublicContent = () => (
  <AppContainer>
    <Navigation />
    <Home />
    <About />
    <Projects />
    <Contact />
  </AppContainer>
);

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={PublicContent} />
          <Route path="/admin" component={AdminContent} />
        </div>
      </Router>
    );
  }
}

export default App;
