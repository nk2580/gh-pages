import React from 'react';
import { Container } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import ParallaxDivider from '../Components/ParallaxDivider';
import Page from '../Components/Page';
import bg from '../Assets/img/bg.jpg';

export default class Home extends React.Component {
  render() {
    return (
      <Page id="Home">
        <ParallaxDivider
        bg={bg}
      >
        <Container className="text-white text-center full-height">
          <h1 className="heading-block vertically-center">
            Nik Kyriakidis<br/>
            <small>Web Developer</small><br/><br/>
            <FontAwesome name="chevron-down" />
          </h1>
        </Container>
      </ParallaxDivider>
    </Page>
    );
  }
}
