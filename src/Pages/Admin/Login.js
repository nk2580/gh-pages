import React, { Component } from 'react';
import PropTypes from "prop-types";
import { 
  Container, 
  Row, 
  Col, 
  Card, 
  CardBody, 
  Button,
  Form,
  Label,
  Input,
  FormGroup,
 } from 'reactstrap';

const headingStyle = {
  textAlign: 'center',
};


class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: "",
      password: "",
    }
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleUsernameChange(event) {
    this.setState({username: event.target.value});
  }

  handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }

  handleSubmit()
  {
    this.props.submitCallback(this.state.username,this.state.password)
  }

  render() {
    return (
        <Container>
        <Row>
          <Col sm={{ size: 6, offset: 3 }}>
            <br/>
            <br/>
            <Card>
              <CardBody>
                <h1 style={headingStyle} >Login</h1>
                <Form>
                  <FormGroup>
                    <Label for="Email">Email</Label>
                    <Input type="email" name="email" id="Email" value={this.state.username} onChange={this.handleUsernameChange} />
                  </FormGroup>
                  <FormGroup>
                    <Label for="Password">Password</Label>
                    <Input type="password" name="password" id="Password" value={this.state.password} onChange={this.handlePasswordChange} />
                  </FormGroup>
                </Form>
                <Button block onClick={this.handleSubmit} >Login</Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
        </Container>
    )
  }
}

Login.proptypes = {
  submitCallback: PropTypes.func.isRequired,
}

export default Login