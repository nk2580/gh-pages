import React, { Component } from 'react';
import { Card, CardBody, CardHeader } from 'reactstrap';
import { Link, Route } from 'react-router-dom'
import PostList from '../../Components/Admin/PostList';
import PostEditor from '../../Components/Admin/PostEditor';


class Posts extends Component {
  render() {
    return (
      <Card>
        <CardHeader>
          <h1>
            Posts
            <Route exact path="/admin/posts" render={() => {
              return <Link to="/admin/posts/new" className="btn btn-primary pull-right">Add New</Link>
            }} />
            <Route exact path="/admin/posts/:subpath(new|edit)" render={() => {
              return <Link to="/admin/posts" className="btn btn-dark pull-right">Back to Posts</Link>
            }} />
          </h1>
        </CardHeader>
        <CardBody>
          <Route exact path="/admin/posts" component={PostList} />
          <Route exact path="/admin/posts/:subpath(new|edit)" component={PostEditor} />
        </CardBody>
      </Card>
    )
  }
}

export default Posts