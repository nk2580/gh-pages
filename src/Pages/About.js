import React from 'react';
import { 
  Container, 
  Row, 
  Col, 
  Card, 
  CardBody,
 } from 'reactstrap';
import ParallaxDivider from '../Components/ParallaxDivider';
import Page from '../Components/Page';
import bg from '../Assets/img/About.jpg';
import me from '../Assets/img/me.jpg';
import { aboutMe } from '../Data';

export default class About extends React.Component {
  render() {
    return (
      <Page id="About">
        <ParallaxDivider 
          bg={bg}
        >
          <Container>
            <h1 className="text-white text-center heading-block">About Me</h1>
          </Container>
        </ParallaxDivider>
        <br/>
        <Container>
          <Card>
            <CardBody>
            <Row>
              <Col md="4" className="text-center">
              <br/>
                <img 
                  src={me}
                  className="img-responsive"
                  alt="Nik Kyriakidis"
                 />
              </Col>
              <Col md="8">
                <div className="text-center">
                    <p>
                      {aboutMe.desciption}
                    </p>
                    <h3>Technical Skills</h3>
                    <h4>Programming Languages</h4>
                    <p>
                      {aboutMe.languages.map((lang, index) => {
                        if (index >= (aboutMe.languages.length -1)) {
                          return lang;
                        }
                        return `${lang} - `;
                      })}
                    </p>
                    <h4>Frameworks</h4>
                    <p>
                      {aboutMe.frameworks.map((lang, index) => {
                        if (index >= (aboutMe.frameworks.length -1)) {
                          return lang;
                        }
                        return `${lang} - `;
                      })}
                    </p>
                    <br />
                    {aboutMe.recommendations.map((rec, index) => 
                      (<blockquote key={index} >
                          {rec.text}
                        <br />
                        <span className="text-italic">
                          {rec.author}
                        </span>
                      </blockquote>)
                    )}

                </div>
              </Col>
            </Row>
            </CardBody>
          </Card>
        </Container>
      </Page>
    );
  }
}
