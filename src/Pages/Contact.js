import React from 'react';
import { Container } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import Page from '../Components/Page';
import { links } from '../Data';


export default class Contact extends React.Component {
  render() {
    return (
      <Page id="Contact">
        <Container className="text-center">
          <hr/>
          {links.map((link, index) => (
            <a key={index} className="text-dark" href={link.link} target="_blank">
            <FontAwesome 
              name={link.icon}
              size="4x"
              />&nbsp;&nbsp;
            </a>
          ))}
    </Container>
        <br/>
      </Page>
    );
  }
}
