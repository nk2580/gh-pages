import React from 'react';
import { Container } from 'reactstrap';
import ParallaxDivider from '../Components/ParallaxDivider';
import Portfolio from '../Components/Portfolio';
import Page from '../Components/Page';
import { PortfolioItems } from '../Data';
import bg from '../Assets/img/Projects.jpg';

export default class Projects extends React.Component {
  render() {
    return (
      <Page id="Projects">
        <ParallaxDivider
          bg={bg}
        >
          <Container>
            <h1 className="text-white text-center heading-block">Projects</h1>
          </Container>
        </ParallaxDivider>
        <br />
        <Container>
          <Portfolio items={PortfolioItems} />
        </Container>
      </Page>
    );
  }
}
