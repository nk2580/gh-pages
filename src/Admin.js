import React, { Component } from 'react';
import cookie from 'react-cookies'
import AdminNavigation from './Components/Admin/AdminNavigation';
import AdminContainer from './Components/Admin/AdminContainer';
import Sidebar from './Components/Admin/Sidebar';
import Login from './Pages/Admin/Login';
import Dashboard from './Pages/Admin/Dashboard';
import Posts from './Pages/Admin/Posts';
import * as AuthClient from './Actions/Auth';
import Users from './Pages/Admin/Users';
import Settings from './Pages/Admin/Settings';
import { Route } from 'react-router-dom'


class Admin extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoggedIn: typeof cookie.load('nk2580auth') !== 'undefined',
      user: null,
    }
    this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
  }

  /**
   * handle the login form sbumission
   * TO-DO: implement the method correctly
   * @param string username 
   * @param string password 
   */
  handleLoginSubmit(username, password) {
    const response = AuthClient.login(username, password);
    this.setState({isLoggedIn: true});
    cookie.save('nk2580auth', 12345);
    console.log(response);
  }

  render() {
    return !this.state.isLoggedIn 
      ? (<Login {...this.props} submitCallback={this.handleLoginSubmit} />)
      : (
        <AdminContainer>
          <AdminNavigation {...this.props} isLoggedIn={this.state.isLoggedIn} />
          <Sidebar {...this.props} />
          <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
              <div>
                <Route exact path="/admin" component={Dashboard} />
                <Route path="/admin/posts" component={Posts} />
                <Route path="/admin/users" component={Users} />
                <Route path="/admin/settings" component={Settings} />
              </div>
          </main>
        </AdminContainer>
    )
  }
}

export default Admin