/**
 * API Client
 */
/**
 * send a post request to the API
 * 
 * @param string url 
 * @param obj data 
 */
export const post = (url, data) => {
  // Default options are marked with *
  return fetch(url, {
    body: JSON.stringify(data), // must match 'Content-Type' header
    headers: {
      'content-type': 'application/json'
    },
    method: 'POST', // *GET, PUT, DELETE, etc.
  })
  .then(response => response.json()) // parses response to JSON
  .catch(error => console.error('Error:', error))
}

/**
 * send a get request to the API
 * 
 * @param string url 
 */
export const get = (url) => {
  // Default options are marked with *
  return fetch(url, {
    method: 'GET', // *GET, PUT, DELETE, etc.
  })
  .then(response => response.json()) // parses response to JSON
  .catch(error => console.error('Error:', error))
}
