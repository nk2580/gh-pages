/**
 * Authentication Action Methods.
 * import these on react components that need to send API Requests for authentication
 */
import * as Api from '../Utilities/ApiClient';

/**
 * API action to perform a login
 * 
 * @param string username 
 * @param string password 
 */
export const login = (username, password) => {
  const response = Api.post('/login', {
    username,
    password
  })
  console.log(response);
  return true;
}

/**
 * Gets a User Object from a Token via the API
 * 
 * @param string userToken 
 */
export const getUserFromToken = (userToken) => {
  const response = Api.post('/login', {userToken})
  console.log(response);
  return {
    userId: 1,
    name: 'example',
  };
}
